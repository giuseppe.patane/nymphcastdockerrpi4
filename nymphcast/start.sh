#!/bin/bash

# SHARED MEMORY
umount /dev/shm && mount -t tmpfs shm /dev/shm

# X SERVER CONFIG
xhost +local:root
export DISPLAY=:0
rm -r /tmp/.X11-unix
rm /tmp/.X0-lock &>/dev/null || true

# HOST CONFIG
echo "127.0.0.1 $HOSTNAME" >> /etc/hosts

# MODPROBES
modprobe bcm2835-v4l2
modprobe v4l2_common

# START X SERVER
 export FRAMEBUFFER=/dev/fb0
 # /usr/local/bin/nymphcast_server -c /home/nymph/NymphCast/src/server/nymphcast_config.ini -a /usr/local/share/nymphcast/apps/ -w /usr/local/share/nymphcast/wallpapers/
 nymphcast_server -c /usr/local/etc/nymphcast/nymphcast_config.ini -a /usr/local/share/nymphcast/apps/ -w /usr/local/share/nymphcast/wallpapers/ &
 /bin/bash
