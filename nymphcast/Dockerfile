FROM balenalib/raspberrypi4-64-debian
#FROM balenalib/%%BALENA_MACHINE_NAME%%-debian:stretch-build

RUN apt update && \
	apt -y upgrade && \
	apt -y install libsdl2-image-dev \
				   libsdl2-dev \
				   libpoco-dev && \
	 apt -y install libswscale-dev \
					libavcodec-dev \
					libavdevice-dev \
					libavformat-dev \
					libavutil-dev \
					libpostproc-dev \
					libswresample-dev \
					build-essential git
RUN apt -y install avahi-daemon \
				   avahi-autoipd \
				   avahi-discover \
				   avahi-dnsconfd \
				   avahi-utils

RUN apt -y install x11-xserver-utils kmod mpv --no-install-recommends


COPY . /home/nymph/
WORKDIR /home/nymph/
RUN if [ -f "/usr/local/lib/libnymphrpc.a" ]; then \
		echo "NymphRPC library found in /usr/local/lib. Skipping installation."; \
	else \
		git clone --depth 1 https://github.com/MayaPosch/NymphRPC.git && \
		echo "Installing NymphRPC..." && \
		make -C NymphRPC/ lib && \
		sudo mkdir -p /usr/local/include/nymph && \
		sudo cp NymphRPC/src/*.h /usr/local/include/nymph/. && \
		sudo chmod 766 /usr/local/include/nymph/* && \
		sudo cp NymphRPC/lib/libnymphrpc.a /usr/local/lib/ && \
		sudo cp NymphRPC/lib/libnymphrpc.so.* /usr/lib/ && \
		sudo chmod 766 /usr/local/lib/libnymphrpc.a && \
		sudo chmod 766 /usr/lib/libnymphrpc.so*; \
		fi && \
		rm -rf NymphRPC && \
		if [ -f "src/server/bin/nymphcast_server" ]; then \
		rm src/server/bin/nymphcast_server; \
		fi
RUN git clone https://github.com/MayaPosch/NymphCast.git
RUN cd NymphCast && \
	make -C src/server && \
	mkdir -p src/server/bin/wallpapers && \
	cp src/server/*.jpg src/server/bin/wallpapers/ && \
	cp src/server/*.ini src/server/bin/ && \
	cp -r src/server/apps src/server/bin/
RUN cd NymphCast && \
	sudo make -C src/server/ install && \
	if [ -d "/run/systemd/system" ]; then \
		sudo make -C src/server/ install-systemd; \
		sudo ln -s /etc/systemd/system/nymphcast.service /etc/systemd/system/multi-user.target.wants/nymphcast.service; \
	else \
		sudo make -C src/server/ install-openrc; \
	fi && \
	sudo install -m 666 src/server/avahi/nymphcast.service /etc/avahi/services/.

RUN echo "deb http://www.deb-multimedia.org buster main non-free" >> /etc/apt/sources.list && \
	sudo apt update -oAcquire::AllowInsecureRepositories=true && \
	sudo apt -y install deb-multimedia-keyring --allow-unauthenticated && \
	sudo apt-get -y install youtube-dl --allow-unauthenticated

ENTRYPOINT ["/usr/bin/entry.sh"]
#CMD ["bash"]
ENV LIBGL_ALWAYS_SOFTWARE=1
CMD ["/home/nymph/start.sh"]

#CMD ["/usr/local/bin/nymphcast_server", "-c", "/home/nymph/NymphCast/src/server/nymphcast_config.ini", "-a", "/usr/local/share/nymphcast/apps/", "-w", "/usr/local/share/nymphcast/wallpapers/"]
