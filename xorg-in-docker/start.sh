#!/bin/bash

# SHARED MEMORY
umount /dev/shm && mount -t tmpfs shm /dev/shm

# X SERVER CONFIG
xhost +local:root
export DISPLAY=:0
rm -r /tmp/.X11-unix
rm /tmp/.X0-lock &>/dev/null || true

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

# HOST CONFIG
echo "127.0.0.1 $HOSTNAME" >> /etc/hosts

# MODPROBES
modprobe bcm2835-v4l2
modprobe v4l2_common

# START X SERVER
 #FRAMEBUFFER=/dev/fb1 startx /usr/app/reflx-os --enable-logging
  #FRAMEBUFFER=/dev/fb1 startx /usr/local/bin/nymphcast_server -c /home/nymph/NymphCast/src/server/nymphcast_config.ini -a /usr/local/share/nymphcast/apps/ -w /usr/local/share/nymphcast/wallpapers/
export FRAMEBUFFER=/dev/fb0
#startx
xinit &
bash
